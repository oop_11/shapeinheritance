/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mutita.shapeinheritance;

/**
 *
 * @author Admin
 */
public class Triangle extends Shape {

    private double base;
    private double height;
    public static final double num = 1.0 / 2;

    public Triangle(double base, double height) {
        this.base = base;
        this.height = height;
    }

    public double calArea() {
        return num * base * height;
    }

    //public void print() {
    //    System.out.println("Triangle base: " + this.base + " height: " + this.height + " area = " + calArea());
   // }

    public double getBase() {
        return base;
    }

    public double getHeight() {
        return height;
    }
    public void print(){
        if(height<=0||base<=0){
            System.out.println("Error: high and base must more than zero!!!");
            return;
        }else {
            System.out.println("Triangle base: " + this.base + " height: " + this.height + " area = " + calArea());
        }
        this.base=base;
        this.height=height;
        
    }
   
}
