/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mutita.shapeinheritance;

/**
 *
 * @author Admin
 */
public class Square extends Rectangle{
    
    public Square(double width, double height) {
        super(width, height);
    }
    @Override
    public double calArea() {
        return super.calArea();
    }



    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }
    public void print(){
         if(width<=0||height<=0){
            System.out.println("Error: side must more than zero!!!");
            return;
        }else{
             System.out.println("Square side: " + this.width + " side: " + this.height + " area = " + calArea());
         }
        this.width=width;
        this.height=height;
    }
}
