/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mutita.shapeinheritance;

/**
 *
 * @author Admin
 */
public class Rectangle extends Shape{
    protected double width;
    protected double height;
    public Rectangle(double width,double height){
        this.height=height;
        this.width=width;
        
    }
    public double calArea() {
        return  width * height;
    }

    

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }
    public void print(){
         if(width<=0||height<=0){
            System.out.println("Error: width and height must more than zero!!!");
            return;
        }else{
             System.out.println("Rectangle width: " + this.width + " height: " + this.height + " area = " + calArea());
         }
        this.width=width;
        this.height=height;
    }
}
